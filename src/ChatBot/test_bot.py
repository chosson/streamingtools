"""
Test bot
"""

import sys
import time
import os
import random
import argparse
import subprocess

import requests

from irc import *
from chatbot import *
from twitch_bot import *


class TestBot(TwitchBot):
	def __init__(self, logs_folder, log_to_console=True, pokemon_exception_handling=False):
		super().__init__(logs_folder, log_to_console, pokemon_exception_handling)

	def repeat_msg(self, cmd, num):
		for i in range(num):
			self.send_privmsg(cmd)
			time.sleep(0.050)

	def send_broken_msg(self):
		self.irc_client.sock.send(b"henlo")


if __name__ == "__main__":
	channel = "chosson"
	# Secrets, so take from environment variables.
	bot_nickname = os.environ["TWITCH_BOT_NAME"]
	bot_password = os.environ["TWITCH_OAUTH_TOKEN"]

	bot = TestBot("logs", True, True)
	bot.connect_and_join(bot_password, bot_nickname, channel)
	time.sleep(1)
	bot.send_broken_msg()
	#bot.repeat_msg("!add_error", 30)
	#time.sleep(0.1)
	#bot.repeat_msg("!say_hi", 30)
	#time.sleep(0.1)
	#bot.repeat_msg("!say_something", 30)
