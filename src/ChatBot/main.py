"""
Main function, runs the bot.
"""


import sys
import os
import argparse

from my_bot import MyBot


def main():
	arg_parser = argparse.ArgumentParser(
		description="Run custom chatbot.",
		epilog="Made by me."
	)
	arg_parser.add_argument(
		"--twitch-channel",
		dest="twitch_channel",
		action="store", nargs="?",
		required=True,
		type=str, metavar="CHAN",
		help="The Twitch channel to join."
	)
	arg_parser.add_argument(
		"--widget-server",
		dest="widget_server",
		action="store", nargs="?",
		required=True,
		type=str, metavar="ADDR",
		help="The custom widget server address (or hostIP:port)."
	)
	arg_parser.add_argument(
		"--show-password",
		dest="show_password",
		action="store_true",
		help="Shows the uncensored password in logs (otherwise censored with *)."
	)

	opts = arg_parser.parse_args(sys.argv[1:])

	channel = opts.twitch_channel
	# Secrets, so take from environment variables.
	bot_nickname = os.environ["TWITCH_BOT_NAME"]
	bot_password = os.environ["TWITCH_OAUTH_TOKEN"]

	bot = MyBot("logs", opts.widget_server, True, True, opts.show_password)
	bot.connect_and_join(bot_password, bot_nickname, channel)
	bot.run()

if __name__ == "__main__":
	main()
