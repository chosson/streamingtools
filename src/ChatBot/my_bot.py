"""
A specific bot linked with the widget server.
"""

import sys
import time
import os
import random
import argparse
import subprocess
import json

import requests
import pynput

from irc import *
from chatbot import *
from twitch_bot import *


class MyBot(TwitchBot):
	def __init__(self, logs_folder, widgets_server_addr, log_to_console=True, pokemon_exception_handling=False, show_password=False):
		super().__init__(logs_folder, log_to_console, pokemon_exception_handling, show_password=show_password)
		self.user_warnings = {}
		self.widgets_server_addr = widgets_server_addr
		self.keyboard = pynput.keyboard.Controller()
		self.hide_cam_key = pynput.keyboard.Key.f21
		self.quotes = json.load(open("quotes.json", "r", encoding="utf-8"))
		alt_help = "OwO but you alweady know how to use halp"
		for name in TwitchBot.HELP_COMMAND_NAMES:
			self.command_methods[name].callback.__doc__ = alt_help

	@TwitchBot.new_command
	def say_hi(self, cmd: Chatbot.CommandData):
		self.send_privmsg("SirUwU henlo fren it me")

	@TwitchBot.new_command
	def say_something(self, cmd: Chatbot.CommandData):
		"""[category]"""
		if cmd.params is not None:
			if cmd.params in self.quotes:
				self.send_privmsg(random.choice(self.quotes[cmd.params]))
			else:
				self.send_privmsg(f"Unrecognized category '{cmd.params}'.")
		else:
			random_category = random.choice(tuple(self.quotes.keys()))
			random_quote = random.choice(self.quotes[random_category])
			self.send_privmsg(random_quote)

	@TwitchBot.new_command(cooldown=5)
	def add_error(self, cmd: Chatbot.CommandData):
		"""[increment]"""
		try:
			inc = cmd.params if cmd.params is not None else "+1"
			req_url = f"http://{self.widgets_server_addr}/commands/counter/num-errors?add={int(inc)}"
			self.logger.info(f"Sending request '{req_url}'")
			requests.post(req_url, data={})
		except ValueError:
			self.send_privmsg(f"sowwy uwu I can't send '{cmd.params}' as an incwement")
		except (requests.exceptions.ConnectionError, requests.exceptions.RequestException) as e:
			self.logger.error(str(e))
			self.send_privmsg(f"sowwy uwu sewvew not tawkin'")

	@TwitchBot.new_command
	def fuck_you(self, cmd: Chatbot.CommandData):
		sender = TwitchBot.get_user(cmd)
		if sender != self.nickname:
			if sender not in self.user_warnings:
				self.user_warnings[sender] = 0
			self.user_warnings[sender] += 1
			num_warnings = self.user_warnings[sender] % 3
			if num_warnings == 0:
				self.send_privmsg(f"SirMad SirSword {sender} is bewing mean. timeout for {sender}!")
				time.sleep(0.1)
				self.send_privmsg(f"/timeout {sender} 20")
			elif num_warnings == 1:
				self.send_privmsg(f"SirPrise {sender} said a bad thing!")
			elif num_warnings == 2:
				self.send_privmsg(f"PunOko that's not vewwy nice {sender}. if you mean again I ban!")

	@TwitchBot.new_command
	def sorry(self, cmd: Chatbot.CommandData):
		sender = TwitchBot.get_user(cmd)
		if sender != self.nickname:
			if sender not in self.user_warnings:
				self.user_warnings[sender] = 0
			if self.user_warnings[sender] == 0:
				self.send_privmsg(f"Why you sowwy {sender} chan?")
			else:
				self.send_privmsg(f"SirUwU ok I fowgive {sender}")
				self.user_warnings[sender] = 0

	@TwitchBot.new_command
	def barnak(self, cmd: Chatbot.CommandData):
		"""[num_swears]"""
		try:
			num_swears = int(cmd.params) if cmd.params is not None else ""
			node_code = f"console.log(require('lorembarnak').getText({num_swears}))"
			proc = subprocess.run(
				"node",
				input=node_code,
				encoding="UTF-8",
				capture_output=True,
				timeout=2
			)
			self.send_privmsg(proc.stdout)
		except ValueError:
			self.send_privmsg(f"sowwy uwu I can't send '{cmd.params}' as an whowole nyumbeww")
		except (OSError, subprocess.TimeoutExpired) as e:
			self.logger.error(str(e))
			self.send_privmsg(f"Oh no it not wowk FeelsBadMan")

	@TwitchBot.new_command(cooldown=5)
	def hide_cam(self, cmd: Chatbot.CommandData):
		self.keyboard.press(self.hide_cam_key)
		time.sleep(0.05)
		self.keyboard.release(self.hide_cam_key)


