"""
Supported widgets

Widget properties are stored in a .json file and the volatile state (such as counter value) is store in a .state file (also JSON format)
"""


import json
from dataclasses import dataclass, replace, asdict
from distutils.util import strtobool


@dataclass
class Counter:
	title: str
	value: int = 0

@dataclass
class Stopwatch:
	title: str
	stops_at_zero: bool = False
	alarm_sound_file: str = None
	value: int = 0
	start: bool = False
	stop: bool = False
	reset: bool = False
	add: int = 0

	def reset_state(self):
		self.value = 0
		self.start = False
		self.stop = False
		self.reset = False
		self.add = 0

@dataclass
class TextBox:
	text_val: str = ""
	numer_val: str = ""


widget_state_fields = {
	Counter: ["value"],
	Stopwatch: ["value"],
	TextBox: []
}


def load_json(filename):
	try:
		return json.load(open(filename, "r"))
	except:
		with open(filename, "w") as file:
			file.write("{}")
		return {}

def save_json(data, filename):
	json.dump(data, open(filename, "w"), ensure_ascii=False, indent=2)

def load_widgets(filename):
	data = load_json(filename)
	widgets = {}
	for widget_class in widget_state_fields.keys():
		widgets[widget_class] = {instance_id: widget_class(**params) for instance_id, params in data[widget_class.__name__].items()}
	return widgets

def load_widget_state(widget_instances, filename):
	for instance_id, fields in load_json(filename).items():
		widget_instances[instance_id] = replace(widget_instances[instance_id], **fields)

def save_widget_state(widget_instances, filename):
	data = {}
	for instance_id, instance in widget_instances.items():
		instance_dict = asdict(instance)
		data[instance_id] = {field_name: instance_dict[field_name] for field_name in widget_state_fields[type(instance)]}
	save_json(data, filename)

def bool_to_js(bool_val):
	return ("false", "true")[int(bool_val)]

def str_to_bool(str_val):
	try:
		result = strtobool(str_val)
	except:
		result = False
	return result
