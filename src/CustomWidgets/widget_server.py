"""
Server class
"""


import sys
from os import path
import argparse
import json
from dataclasses import *

import numpy as np
import scipy.io.wavfile
import scipy as sp
import flask
import flask_socketio
from bs4 import BeautifulSoup
import sounddevice as sd

from widgets import *


class CustomWidgetsServer:
	def __init__(self, widgets_filename, **kwargs):
		self.flask_app_args = {
			"static_url_path": ""
		}
		self.flask_app_args.update(kwargs)
		self.widgets_filename = widgets_filename
		self.options = {}
		self.app = None
		self.socketio = None

	def parse_args(self, args=None):
		arg_parser = argparse.ArgumentParser(
			description="Run custom widgets server with a commands page.",
			epilog="Made by me."
		)
		arg_parser.add_argument(
			"--host",
			dest="host",
			action="store", nargs=1,
			type=str, metavar="HOST",
			default="0.0.0.0",
			help="The host IP address to use."
		)
		arg_parser.add_argument(
			"--port",
			dest="port",
			action="store", nargs=1,
			type=int, metavar="PORT",
			default=42069,
			help="The TCP port to use."
		)
		arg_parser.add_argument(
			"--debugger",
			dest="debugger",
			action="store", nargs=1,
			type=str, metavar="DEBUGGER",
			choices=["internal", "external", "none"],
			default="none",
			help="Which type of debugger to use."
		)
		arg_parser.add_argument(
			"--static-folder",
			dest="static_folder",
			action="store", nargs=1,
			type=str, metavar="FOLDER",
			default=".",
			help="Folder containing static files. This is used as the static_folder for Flask. Defaults to the current directory."
		)
		arg_parser.add_argument(
			"--template-folder",
			dest="template_folder",
			action="store", nargs=1,
			type=str, metavar="FOLDER",
			default=".",
			help="Folder containing Flask template files. This is used as the template_folder for Flask. Defaults to the current directory."
		)
		arg_parser.add_argument(
			"--audio-dev",
			dest="audio_dev",
			action="store", nargs=1,
			type=str, metavar="DEVICE",
			default=None,
			help="Audio device for alerts playback."
		)

		self.options.update(vars(arg_parser.parse_args(args if args is not None else sys.argv[1:])))
		self.flask_app_args["static_folder"] = path.relpath(self._get_option("static_folder")).replace("\\", "/")
		self.flask_app_args["template_folder"] = path.relpath(self._get_option("template_folder")).replace("\\", "/")

	def setup(self):
		self.app = flask.Flask(__name__.split(".")[0], **self.flask_app_args)
		self.socketio = flask_socketio.SocketIO(self.app)
		self._load_widgets(self.widgets_filename)
		self._dispatch_methods()
		audio_dev = self._get_option("audio_dev")
		if audio_dev is not None:
			sd.default.device = (sd.default.device[0], audio_dev)

	def run(self):
		host_addr = self._get_option("host")
		tcp_port = self._get_option("port")
		debug_opts, msg = self._get_debug_opts()
		print(msg)
		#self.app.run(host=host_addr, port=tcp_port, **debug_opts)
		self.socketio.run(self.app, host=host_addr, port=tcp_port, **debug_opts)

	def index(self):
		return "<h1>Nothing here mate.</h1>"

	def commands(self):
		return flask.render_template("commands.html")

	def commands_counter(self, id):
		query = flask.request.args
		load_widget_state(self.counters, "Counter.state")
		if id not in self.counters:
			return "", 404
		counter = self.counters[id]
		if "add" in query:
			counter.value += int(query.get("add"))
		elif "reset" in query:
			reset_val = query.get("reset")
			counter.value = int(reset_val) if reset_val != "" else 0
		save_widget_state(self.counters, "Counter.state")
		
		if self.socketio is not None:
			socket_namespace = f"/widgets/counter/{id}"
			event_name=f"counter.update"
			self.socketio.emit(
				event_name,
				{"value": counter.value},
				namespace=socket_namespace
			)
		
		return ""

	def commands_stopwatch(self, id):
		query = flask.request.args
		if id not in self.stopwatches:
			return "", 404
		stopwatch = self.stopwatches[id]
		stopwatch.start = "start" in query
		stopwatch.stop = "stop" in query
		stopwatch.reset = "reset" in query
		stopwatch.add = int(query.get("add", 0))

		if self.socketio is not None:
			socket_namespace = f"/widgets/stopwatch/{id}"
			event_name=f"stopwatch.update"
			self.socketio.emit(
				event_name,
				asdict(stopwatch),
				namespace=socket_namespace
			)

		return ""

	def textbox(self, id):
		query = flask.request.args
		if id not in self.textboxes:
			return "<h1>bruh</h1>", 404
		return flask.render_template(
			"textbox.html",
			textbox_id=id,
			text_val=self.textboxes[id].text_val,
			numer_val=self.textboxes[id].numer_val,
			show_box=str_to_bool(query.get("show_box", str(False)).strip())
		)

	def counter(self, id):
		query = flask.request.args
		if id not in self.counters:
			return "<h1>bruh</h1>", 404
		if "update" in query:
			# This is the GET polling interface. It still works, but using Socket.IO should be prioritized.
			return flask.jsonify(value=self.counters[id].value)

		socket_namespace = f"/widgets/counter/{id}"
		event_name=f"counter.update"
		return flask.render_template(
			"counter.html",
			counter_id=id,
			title=self.counters[id].title,
			value="???",
			socket_namespace=socket_namespace,
			event_name=event_name
		)

	def stopwatch(self, id):
		query = flask.request.args
		if id not in self.stopwatches:
			return "<h1>bruh</h1>", 404
		sound_filename = self.stopwatches[id].alarm_sound_file
		if "countdown-ended" in query and sound_filename is not None:
			self.app.logger.info(f"Alarm caused by stopwatch '{id}'")
			sr, samples = sp.io.wavfile.read(self._get_static_file(sound_filename))
			sd.play(samples.astype(float)/(2**15-1), sr)
			self.app.logger.info(f"Played sound '{sound_filename}'")
			return ""
		if "update" in query:
			# This is the GET polling interface. It still works, but using Socket.IO should be prioritized.
			state = asdict(self.stopwatches[id])
			self.stopwatches[id].reset_state()
			return flask.jsonify(**state)

		socket_namespace = f"/widgets/stopwatch/{id}"
		event_name=f"stopwatch.update"
		return flask.render_template(
			"stopwatch.html",
			stopwatch_id=id,
			title=self.stopwatches[id].title,
			stops_at_zero=bool_to_js(self.stopwatches[id].stops_at_zero),
			socket_namespace=socket_namespace,
			event_name=event_name
		)

	def _get_option(self, key):
		value = self.options.get(key, None)
		if value is not None and isinstance(value, list) and len(value) == 1:
			return value[0]
		else:
			return value

	def _get_static_file(self, filename):
		return path.relpath(path.join(self.app.static_folder, filename))

	def _load_widgets(self, widgets_filename):
		self.widgets = load_widgets(widgets_filename)
		for widget_class in widget_state_fields.keys():
			load_widget_state(self.widgets[widget_class], widget_class.__name__ + ".state")
		self.counters = self.widgets[Counter]
		self.stopwatches = self.widgets[Stopwatch]
		self.textboxes = self.widgets[TextBox]

	def _get_debug_opts(self):
		debugger = self._get_option("debugger")
		debug_opts, msg = {}, ""
		if debugger == "external":
			# If using socketio, there is no internal debugger, the `use_debugger` option is only useful for raw Flask server, not one started throught Flask-SocketIO
			#debug_opts = {"debug": True, "use_reloader": False, "use_debugger": False}
			debug_opts = {"debug": True, "use_reloader": False}
			msg = "Starting with external debugger"
		elif debugger == "internal":
			debug_opts = {"debug": True}
			msg = "Starting with internal debugger"
		else:
			debug_opts = {"debug": False}
			msg = "Starting without debugging"
		return debug_opts, msg

	def _dispatch_methods(self):
		self.app.add_url_rule("/", view_func=self.index)
		self.app.add_url_rule("/index", view_func=self.index)
		self.app.add_url_rule("/commands", view_func=self.commands)
		self.app.add_url_rule("/commands/counter/<id>", methods=["GET", "POST"], view_func=self.commands_counter)
		self.app.add_url_rule("/commands/stopwatch/<id>", methods=["GET", "POST"], view_func=self.commands_stopwatch)
		self.app.add_url_rule("/textbox/<id>", view_func=self.textbox)
		self.app.add_url_rule("/counter/<id>", view_func=self.counter)
		self.app.add_url_rule("/stopwatch/<id>", view_func=self.stopwatch)

