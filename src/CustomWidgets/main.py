"""
Main function, runs the server.
"""


import sys

from widget_server import CustomWidgetsServer


def main():
	server = CustomWidgetsServer("widgets.json")
	sys.argv += ["--static-folder", "../../assets", "--template-folder", "../../assets/html/widgets/templates"]
	server.parse_args()
	server.setup()
	server.run()

if __name__ == "__main__":
	main()
